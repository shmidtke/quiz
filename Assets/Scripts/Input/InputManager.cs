﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{

    public static float Horizontal;
    public static bool Jump;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        Jump = Input.GetButtonDown("Jump");
#else
        Jump = Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
#endif
    }

    void FixedUpdate()
    {
#if UNITY_EDITOR
        Horizontal = Input.GetAxis("Horizontal");
#else
        Horizontal = Input.acceleration.x;
#endif
    }
}
