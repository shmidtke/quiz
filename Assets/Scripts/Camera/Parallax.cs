﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public class Parallax : MonoBehaviour
{
    [Tooltip("Camera to use for the parallax.")]
    public Camera parallaxCamera;

    public float ParallaxScale = 10.0f;

    public float ParallaxSmoothing = 5f;

    [Tooltip("The elements in the parallax.")]
    public List<ParallaxElement> Elements;


    private Vector3 previousCamPos;
    private CameraController cameraController;


    void Start()
    {
        cameraController = parallaxCamera.GetComponent<CameraController>();
        cameraController.OnPositionChanged += OnCameraPositionChanged;

        previousCamPos = parallaxCamera.transform.position;
    }

    private void OnCameraPositionChanged(Vector3 position)
    {
        foreach (var parallaxElement in Elements)
        {
            var parallax = (previousCamPos.x - position.x) * parallaxElement.DeepnessRatio * ParallaxScale;
            parallaxElement.ApplyParallax(parallax, ParallaxSmoothing);
        }

        previousCamPos = position;
    }

    [Serializable]
    public class ParallaxElement
    {
        [Tooltip("Game objects to parallax.")]
        public List<Transform> GameObjects;

        [Tooltip("The deepness of each background object. 0 - the deepest, 1 - the nearest.")]
        [Range(0.0f, 1.0f)]
        public float DeepnessRatio;

        public void ApplyParallax(float parallax, float smoothing)
        {
            foreach (var t in GameObjects)
            {
                float backgroundTargetPosX = t.position.x + parallax;

                // Create a target position which is the background's current position but with it's target x position.
                Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, t.position.y, t.position.z);

                // Lerp the background's position between itself and it's target position.
                t.position = Vector3.Lerp(t.position, backgroundTargetPos, smoothing * Time.deltaTime);
            }
        }
    }
}